package StepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class youtubeMusic {
    WebDriver driver;

    @Given("I Open browser")
    public void iOpenBrowser() {
        final String dir = System.getProperty("user.dir");
        System.out.println("current dir = " + dir);
        System.setProperty("webdriver.chrome.driver", dir + "/driver/chromedriver");
        driver = new ChromeDriver();
    }
    @And("Input the url on the address bar")
    public void openWebsiteGoogle() throws InterruptedException {
        driver.get("https://music.youtube.com/");
        driver.manage().window().maximize();
        Thread.sleep(1000);
    }
    @Then("Click Search menu")
    public void thenClickSearchMenu() {
        driver.findElement(By.xpath("//ytmusic-search-box")).click();
    }
    @And("Input text {string}")
    public void inputTheName(String searchValue) {
        driver.findElement(By.xpath("(//input[@id=\"input\"])")).sendKeys(searchValue);
    }
    @Then("Click enter")
    public void clickEnter() throws InterruptedException {
        driver.findElement(By.xpath("(//input[@id=\"input\"])")).sendKeys(Keys.ENTER);
        Thread.sleep(3000);
    }
    @Then("User should find the artist based on the search")
    public void userShouldFindTheArtistBasedOnTheSearch() throws InterruptedException {
        driver.findElement(By.xpath("(//a[@aria-label=\"Slipknot\"])[2]")).isDisplayed();
//        String result = driver.findElement(By.xpath("(//a[@aria-label=\"Slipknot\"])[2]")).getText();
//        System.out.println(result);
        driver.close();
        driver.quit();
    }
}