Feature: Youtube Music Search
  Scenario: TC005 - User should able to use search function
    Given I Open browser
    And Input the url on the address bar
    Then Click Search menu
    And Input text "Slipknot"
    Then Click enter
    Then User should find the artist based on the search