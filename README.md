# Efishery Test Demo


**The Tools Needed**
- Download [IntelliJ IDEA](https://www.jetbrains.com/idea/download/)
- Download Web Driver
    - [Chromedriver](https://chromedriver.chromium.org/downloads)
    - [Geckodriver](https://github.com/mozilla/geckodriver/releases)
- Download [JAVA] https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html

**Installation**
- Install Intellij IDEA
- Install JAVA
- Adding JAVA into our bash_profile or zsh_profile profile
    - Open Terminal
    - Type sudo nano ~/.bash_profile or ~./zsh_profile
    - Add PATH JAVA below
        - export JAVA_HOME=$(/usr/libexec/java_home -v 1.8.0_333)
        - export PATH="$PATH:$JAVA_HOME/bin"
- Close the bash_profile dont forget to save
- On terminal type source ~/.bash_profile

**Start New Project**
1. Create New Project using MAVEN
2. Install Plugin Cucumber and Gherkin
    - Go to Preferences
    - Go to Plugins
    - Searching for Cucumber and also Gherkin and perform install
3. Put all dependecies on your pom.xml 

    ```xml
        <?xml version="1.0" encoding="UTF-8"?>
        <project xmlns="http://maven.apache.org/POM/4.0.0"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
        <modelVersion>4.0.0</modelVersion>

        <groupId>org.example</groupId>
        <artifactId>TestDemo</artifactId>
        <version>1.0-SNAPSHOT</version>

        <properties>
            <maven.compiler.source>8</maven.compiler.source>
            <maven.compiler.target>8</maven.compiler.target>
            <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        </properties>

        <dependencies>

            <!-- https://mvnrepository.com/artifact/io.cucumber/cucumber-java -->
            <dependency>
                <groupId>io.cucumber</groupId>
                <artifactId>cucumber-java</artifactId>
                <version>6.9.0</version>
            </dependency>

            <!-- https://mvnrepository.com/artifact/junit/junit -->
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>4.13.1</version>
                <scope>test</scope>
            </dependency>

            <!-- https://mvnrepository.com/artifact/io.cucumber/cucumber-junit -->
            <dependency>
                <groupId>io.cucumber</groupId>
                <artifactId>cucumber-junit</artifactId>
                <version>6.9.0</version>
                <scope>test</scope>
            </dependency>

            <!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java -->
            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>selenium-java</artifactId>
                <version>3.141.59</version>
            </dependency>

            <!-- https://mvnrepository.com/artifact/net.masterthought/cucumber-reporting -->
            <dependency>
                <groupId>net.masterthought</groupId>
                <artifactId>cucumber-reporting</artifactId>
                <version>5.4.0</version>
            </dependency>


        </dependencies>

        <build>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>2.22.0</version>
                    <configuration>
                        <testFailureIgnore>true</testFailureIgnore>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>net.masterthought</groupId>
                    <artifactId>maven-cucumber-reporting</artifactId>
                    <version>2.8.0</version>
                    <executions>
                        <execution>
                            <id>execution</id>
                            <phase>verify</phase>
                            <goals>
                                <goal>generate</goal>
                            </goals>
                            <configuration>
                                <projectName>automation-bdd-google</projectName>
                                <outputDirectory>${project.build.directory}/cucumber-report-html</outputDirectory>
                                <cucumberOutput>${project.build.directory}/cucumber.json</cucumberOutput>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </build>
    </project>
    ```

4. Create new directory with name driver. Add put your webdriver inside the folder /driver/chromedirver or /driver/geckodriver.
5. Create new directory called resources /src/test/resources/features and create feature file e.g youtubeMusic.feature and put on folder /src/test/resources/features
6. Put Gherkin style under file youtubeMusic.feature
    ```gherkin
            Feature: Go to YoutubeMusic Page
            Scenario: TC005 - User should able to use search function
            Given I Open browser
            Then Input the url on the address bar
            Then Click Search menu
            And Input text "Slipknot"
            Then Click enter
            Then User should find the artist based on the search
    ```

7. Add new package "StepDef" and "TestRunner" on folder /src/test/java
8. Create java class "youtubeMusic" and put it into package /src/test/java/StepDef
    ```java
            package StepDef;

            import io.cucumber.java.en.And;
            import io.cucumber.java.en.Given;
            import io.cucumber.java.en.Then;
            import org.openqa.selenium.By;
            import org.openqa.selenium.Keys;
            import org.openqa.selenium.WebDriver;
            import org.openqa.selenium.chrome.ChromeDriver;

            public class youtubeMusic {
                WebDriver driver;

                @Given("I Open browser")
                public void iOpenBrowser() {
                    final String dir = System.getProperty("user.dir");
                    System.out.println("current dir = " + dir);
                    System.setProperty("webdriver.chrome.driver", dir + "/driver/chromedriver");
                    driver = new ChromeDriver();
                }
                @Then("Input the url on the address bar")
                public void openWebsiteGoogle() throws InterruptedException {
                    driver.get("https://music.youtube.com/");
                    driver.manage().window().maximize();
                    Thread.sleep(1000);
                }
                @Then("Click Search menu")
                public void thenClickSearchMenu() {
                    driver.findElement(By.xpath("//ytmusic-search-box")).click();
                }
                @And("Input text {string}")
                public void inputTheName(String searchValue) {
                    driver.findElement(By.xpath("(//input[@id=\"input\"])")).sendKeys(searchValue);
                }
                @Then("Click enter")
                public void clickEnter() throws InterruptedException {
                    driver.findElement(By.xpath("(//input[@id=\"input\"])")).sendKeys(Keys.ENTER);
                    Thread.sleep(3000);
                }
                @Then("User should find the artist based on the search")
                public void userShouldFindTheArtistBasedOnTheSearch() throws InterruptedException {
                    driver.findElement(By.xpath("(//a[@aria-label=\"Slipknot\"])[2]")).isDisplayed();
                    driver.close();
                    driver.quit();
                }
            }
    ```

9. Create java class "TestRunner" and put into /src/test/java/TestRunner
    ```java
                package TestRunner;

                import io.cucumber.junit.Cucumber;
                import io.cucumber.junit.CucumberOptions;
                import org.junit.runner.RunWith;

                @RunWith(Cucumber.class)
                @CucumberOptions(features ="src/test/resources/features",
                        glue = {"StepDef"},
                        plugin = {"pretty","json:target/cucumber.json"})
                public class TestRunner {
                }
    ```

10. Go to terminal and type "mvn test" for run the script
11. You can check the report on /target/cucumber.json or go to /target/cucumber-html-reports/file-src-test-resources-features-youtubeMusic-feature.html